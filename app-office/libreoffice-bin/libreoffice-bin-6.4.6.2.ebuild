# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit xdg-utils

MY_PN="${PN%-bin}"
MY_PV="$(ver_cut 1-3)"
MY_P="${MY_PN}$(ver_cut 1-2)"

DESCRIPTION="A full office productivity suite. Binary package"
HOMEPAGE="https://www.libreoffice.org/"
SRC_URI="${SRC_URI}
	https://download.documentfoundation.org/libreoffice/stable/${MY_PV}/deb/x86_64/LibreOffice_${MY_PV}_Linux_x86-64_deb.tar.gz
		-> ${P}.tar.gz
"

LICENSE="MPL-2.0"
SLOT="0"
KEYWORDS="amd64"
IUSE="${IUSE}
	gnome
	kde
"

RDEPEND="${RDEPEND}
	net-dns/avahi
	virtual/jre
"

QA_PREBUILT="opt/${MY_P}/program/*"

S="${WORKDIR}/LibreOffice_${PV}_Linux_x86-64_deb"

src_prepare() {
	default

	if use !gnome; then
		rm DEBS/*gnome-integration*.deb
	fi

	if use !kde; then
		rm DEBS/*kde-integration*.deb
	fi
}

src_install() {
	for file in DEBS/*.deb; do
		if [[ "${file}" =~ debian-menus ]]; then
			continue
		fi

		ar -x "${file}"
		tar -ox -f data.tar.xz -C "${ED}"
	done

	ar -x DEBS/*debian-menus*
	tar -ox -f data.tar.xz

	insinto /usr/bin
	doins "usr/local/bin/${MY_P}"

	insinto /usr/share
	doins -r usr/share/application-registry
	doins -r usr/share/applications
	doins -r usr/share/icons
	doins -r usr/share/mime
	doins -r usr/share/mime-info

	insinto /usr/share/metainfo
	doins usr/share/appdata/*
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
}
