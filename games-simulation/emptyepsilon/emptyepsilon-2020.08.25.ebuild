# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake desktop

MY_PN="EmptyEpsilon"
MY_PV="EE-${PV}"
MY_P="${MY_PN}-${MY_PV}"

DESCRIPTION="Open source bridge simulator"
HOMEPAGE="http://daid.github.io/EmptyEpsilon/"
SRC_URI+=" https://github.com/daid/EmptyEpsilon/archive/EE-${PV}.tar.gz -> ${P}.tar.gz"
SRC_URI+=" https://github.com/daid/SeriousProton/archive/EE-${PV}.tar.gz -> ${P}-seriousproton.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE+=" doc"
IUSE+=" libglvnd"

DEPEND+=" media-libs/libsfml"
DEPEND+=" libglvnd? ( media-libs/libglvnd )"
RDEPEND+=" ${DEPEND}"
BDEPEND+=" "

S="${WORKDIR}/${MY_P}"

src_prepare() {
	cmake_src_prepare

	sed -i '/install(.*script_reference.html/d' CMakeLists.txt
}

src_configure() {
	local mycmakeargs=(
		-DSERIOUS_PROTON_DIR="${WORKDIR}/SeriousProton-${MY_PV}"
		-DCPACK_PACKAGE_VERSION_MAJOR="$(ver_cut 1)"
		-DCPACK_PACKAGE_VERSION_MINOR="$(ver_cut 2)"
		-DCPACK_PACKAGE_VERSION_PATCH="$(ver_cut 3)"
		-DOpenGL_GL_PREFERENCE="$(usex libglvnd GLVND LEGACY)"
	)

	cmake_src_configure
}

src_install() {
	if use doc; then
		HTML_DOCS+=" script_reference.html"
	fi

	cmake_src_install

	newicon resources/logo_white.png "${PN}.png"
	make_desktop_entry EmptyEpsilon EmptyEpsilon
}
